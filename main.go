package main

import (
	"os"

	"gitlab.com/varex83/authenticator/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
