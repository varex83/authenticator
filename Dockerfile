FROM golang:1.17

WORKDIR $GOPATH/src/gitlab.com/varex83/authenticator

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/authenticator gitlab.com/varex83/authenticator


###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/authenticator /usr/local/bin/authenticator
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["authenticator"]
