/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type User struct {
	Email *string `json:"email,omitempty"`
	Id    *int32  `json:"id,omitempty"`
	// SHA256 hashed password
	Password *string `json:"password,omitempty"`
	Role     *string `json:"role,omitempty"`
}
