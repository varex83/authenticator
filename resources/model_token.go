/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type Token struct {
	Expiration *int32  `json:"expiration,omitempty"`
	Id         *int32  `json:"id,omitempty"`
	Token      *string `json:"token,omitempty"`
}
