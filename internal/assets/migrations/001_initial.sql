-- +migrate Up
create table accounts (
    id bigserial not null unique,
    email varchar(1024) not null unique,
    pass varchar(1024) not null,
    role int not null
);
create table tokens (
    id int,
    token varchar(1024) not null unique, 
    expiration bigint not null,
    constraint fk_id
      foreign key(id) 
	  references accounts(id)
);
create table refresh_tokens (
    token varchar(1024),
    constraint fk_token
        foreign key(token)
            references tokens(token),
    refresh_token varchar(1024) not null unique,
    expiration bigint not null
);

-- +migrate Down
drop table refresh_token;
drop table tokens; 
drop table accounts;