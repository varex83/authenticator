package service

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/varex83/authenticator/internal/data/pg"
	"gitlab.com/varex83/authenticator/internal/service/ctx"
	"gitlab.com/varex83/authenticator/internal/service/handlers"
	"gitlab.com/varex83/authenticator/internal/service/middleware"
)

func (s *service) router() chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.ContentType("application/vnd.api+json"),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			ctx.CtxLog(s.log),
			ctx.CtxAccountsDB(pg.NewAccountQ(s.db)),
			ctx.CtxTokensDB(pg.NewTokenQ(s.db)),
			ctx.CtxRefreshTokensDB(pg.NewRTokenQ(s.db)),
			ctx.CtxCfg(s.config),
		),
	)
	r.Route("/api/v1", func(r chi.Router) {
		r.Get("/account_positions", handlers.GetTrades)
		r.Get("/login", handlers.Login)
		r.Group(func(r chi.Router) {
			r.Use(ctx.Tokenizer)
			r.Group(func(r chi.Router) {
				r.Use(ctx.RTokenizer)
				r.Get("/refresh", handlers.Refresh)
			})
			r.Group(func(r chi.Router) {
				r.Use(ctx.TokenAuth)
				r.Get("/change_password", handlers.ChangePassword)
				r.Group(func(r chi.Router) {
					r.Use(middleware.AdminLogin)
					r.Get("/create_account", handlers.CreateAccount)
				})
			})
		})
	})
	return r
}
