package ctx

import (
	"context"
	"net/http"

	"gitlab.com/varex83/authenticator/internal/data"
)

func CtxTokensDB(db data.TokenQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, tokensCtxKey, db)
	}
}

func TokensDB(r *http.Request) data.TokenQ {
	return r.Context().Value(tokensCtxKey).(data.TokenQ)
}
