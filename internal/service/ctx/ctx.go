package ctx

type ctxKey int

const (
	logCtxKey ctxKey = iota
	tokensCtxKey
	accountsCtxKey
	tokenCtxKey
	tokenExpPeriod
	refreshTokenExpPeriod
	rTokensCtxKey
	rTokensDbCtxKey
	apiKeyCtxKey
	apiSecretKetCtxKey
)
