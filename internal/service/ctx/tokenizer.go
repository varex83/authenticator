package ctx

import (
	"context"
	"errors"
	"net/http"
	"strings"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func getToken(r *http.Request) (string, error) {
	str := r.Header.Get("Authorization")
	strParam := strings.Split(str, " ")
	if len(strParam) < 2 {
		return "", errors.New("1 token haven't found")
	}
	if strParam[0] != "Bearer" {
		return "", errors.New("token haven't found")
	}
	return strParam[1], nil
}

func Tokenizer(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, err := getToken(r)
		if err != nil {
			Log(r).Error(err)
			ape.RenderErr(w, problems.BadRequest(err)...)
			return
		}
		r = r.WithContext(context.WithValue(r.Context(), tokenCtxKey, token))
		next.ServeHTTP(w, r)
	})
}

func GetToken(r *http.Request) string {
	return r.Context().Value(tokenCtxKey).(string)
}
