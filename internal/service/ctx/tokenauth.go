package ctx

import (
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func TokenAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logrus.Println("Token Auth")
		token := GetToken(r)
		tokky, err := TokensDB(r).GetByToken(token)
		if err != nil || tokky.Expiration <= time.Now().Unix() {
			ape.RenderErr(w, problems.Unauthorized())
			return
		}
		next.ServeHTTP(w, r)
	})
}
