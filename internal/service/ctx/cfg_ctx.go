package ctx

import (
	"context"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/varex83/authenticator/internal/config"
)

func CtxCfg(config config.Config) func(context.Context) context.Context {
	configMap, err := config.AppConfig()
	if err != nil {
		logrus.Fatal(err)
	}
	expPeriod := configMap["token_expiration"]
	rExpPeriod := configMap["refresh_token_expiration"]
	apiKey := configMap["api_key"]
	apiSecret := configMap["api_secret"]
	return func(ctx context.Context) context.Context {
		ctx = context.WithValue(ctx, tokenExpPeriod, expPeriod)
		ctx = context.WithValue(ctx, refreshTokenExpPeriod, rExpPeriod)
		ctx = context.WithValue(ctx, apiKeyCtxKey, apiKey)
		ctx = context.WithValue(ctx, apiSecretKetCtxKey, apiSecret)
		return ctx
	}
}

func GetTokenExp(r *http.Request) int {
	return r.Context().Value(tokenExpPeriod).(int)
}

func GetRefreshTokenExp(r *http.Request) int {
	return r.Context().Value(refreshTokenExpPeriod).(int)
}

func GetApiKey(r *http.Request) string {
	return r.Context().Value(apiKeyCtxKey).(string)
}

func GetApiSecret(r *http.Request) string {
	return r.Context().Value(apiSecretKetCtxKey).(string)
}
