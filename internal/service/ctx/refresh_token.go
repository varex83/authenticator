package ctx

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/varex83/authenticator/internal/service/requests"
)

func RTokenizer(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := requests.GetRToken(r)

		rtoken, err := RTokensDB(r).GetByRToken(token)

		if err != nil {
			Log(r).Error(err)
			ape.RenderErr(w, problems.BadRequest(err)...)
			return
		}
		if rtoken.Token != GetToken(r) || rtoken.Expiration <= time.Now().Unix() {
			Log(r).Error(err)
			ape.RenderErr(w, problems.Unauthorized())
			return
		}

		r = r.WithContext(context.WithValue(r.Context(), rTokensCtxKey, token))
		next.ServeHTTP(w, r)
	})
}

func GetRToken(r *http.Request) string {
	return r.Context().Value(rTokensCtxKey).(string)
}
