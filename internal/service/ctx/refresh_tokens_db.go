package ctx

import (
	"context"
	"net/http"

	"gitlab.com/varex83/authenticator/internal/data"
)

func CtxRefreshTokensDB(db data.RTokenQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, rTokensDbCtxKey, db)
	}
}

func RTokensDB(r *http.Request) data.RTokenQ {
	return r.Context().Value(rTokensDbCtxKey).(data.RTokenQ)
}
