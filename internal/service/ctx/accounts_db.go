package ctx

import (
	"context"
	"net/http"

	"gitlab.com/varex83/authenticator/internal/data"
)

func CtxAccountsDB(db data.AccountQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, accountsCtxKey, db)
	}
}

func AccountsDB(r *http.Request) data.AccountQ {
	return r.Context().Value(accountsCtxKey).(data.AccountQ)
}
