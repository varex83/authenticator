package requests

import "net/http"

func GetOldPass(r *http.Request) string {
	return r.FormValue("old_password")
}
