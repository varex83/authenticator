package requests

import (
	"net/http"

	"gitlab.com/varex83/authenticator/internal/data"
)

func GetAccount(r *http.Request) data.Account {
	res := data.Account{
		Email:    r.FormValue("email"),
		Password: r.FormValue("pass"),
	}
	if r.FormValue("role") == "admin" {
		res.Role = data.Admin
	} else {
		res.Role = data.General
	}
	return res
}
