package requests

import "net/http"

func GetRToken(r *http.Request) string {
	return r.FormValue("refresh_token")
}
