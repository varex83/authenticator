package requests

import "net/http"

func GetNewPass(r *http.Request) string {
	return r.FormValue("new_password")
}
