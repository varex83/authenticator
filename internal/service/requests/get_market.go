package requests

import (
	"net/http"

	"github.com/go-chi/chi"
)

func GetMarket(r *http.Request) string {
	return chi.URLParam(r, "market")
}
