package handlers

import (
	"errors"
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/varex83/authenticator/internal/service/ctx"
	"gitlab.com/varex83/authenticator/internal/service/requests"
	"gitlab.com/varex83/authenticator/internal/service/tokens"
	"golang.org/x/crypto/bcrypt"
)

func ChangePassword(w http.ResponseWriter, r *http.Request) {
	token := ctx.GetToken(r)
	pass := requests.GetNewPass(r)
	newPasswordb, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	newPassword := string(newPasswordb)

	if newPassword == "" {
		err = errors.New("new password not provided")
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	account, err := tokens.GetAccountByToken(r, token)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		ctx.Log(r).Error(err)
		return
	}

	old_pass := requests.GetOldPass(r)
	if bcrypt.CompareHashAndPassword([]byte(account.Password), []byte(old_pass)) != nil {
		ctx.Log(r).Error(problems.Unauthorized())
		ape.RenderErr(w, problems.Unauthorized())
		return
	}

	account.Password = newPassword
	err = ctx.AccountsDB(r).Update(account)
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	tokens, err := ctx.TokensDB(r).Get(uint64(account.ID))
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	for _, v := range tokens {
		ctx.RTokensDB(r).Reset()
		ctx.RTokensDB(r).FilterByToken(v.Token)
		err = ctx.RTokensDB(r).Delete()
		if err != nil {
			ctx.Log(r).Error(err)
			ape.RenderErr(w, problems.InternalError())
			return
		}
		ctx.RTokensDB(r).Reset()
	}
	ctx.TokensDB(r).FilterById(account.ID)
	err = ctx.TokensDB(r).Delete()
	ctx.TokensDB(r).Reset()
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}

	ape.Render(w, account)
}
