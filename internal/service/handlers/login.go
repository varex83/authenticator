package handlers

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/varex83/authenticator/internal/service/ctx"
	"gitlab.com/varex83/authenticator/internal/service/requests"
	"gitlab.com/varex83/authenticator/internal/service/tokens"
	"golang.org/x/crypto/bcrypt"
)

func Login(w http.ResponseWriter, r *http.Request) {
	accountFromR := requests.GetAccount(r)
	account, err := ctx.AccountsDB(r).Get(accountFromR.Email)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	if bcrypt.CompareHashAndPassword([]byte(account.Password), []byte(accountFromR.Password)) == nil {
		tokenPair := tokens.GenTokenPair(r, account)
		err := ctx.TokensDB(r).Insert(tokenPair.Token)
		if err != nil {
			logrus.Error(err)
			ape.RenderErr(w, problems.BadRequest(err)...)
			return
		}
		err = ctx.RTokensDB(r).Insert(tokenPair.RefreshToken)
		if err != nil {
			logrus.Error(err)
			ape.RenderErr(w, problems.BadRequest(err)...)
			return
		}
		ape.Render(w, tokenPair)
		return
	} else {
		ape.RenderErr(w, problems.Unauthorized())
		return
	}
}
