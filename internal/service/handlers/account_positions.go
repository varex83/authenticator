package handlers

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/varex83/authenticator/internal/service/ctx"
)

const (
	ftxApi = "https://ftx.com/api/positions"
)

func GetTrades(w http.ResponseWriter, r *http.Request) {
	hash := hmac.New(sha256.New, []byte(ctx.GetApiSecret(r)))
	curr_time := int(time.Now().Unix() * 1000)
	signBody := strconv.Itoa(curr_time) + "GET" + "/api/positions"
	hash.Write([]byte(signBody))
	req, err := http.NewRequest("GET", ftxApi, nil)

	if err != nil {
		ape.RenderErr(w, problems.Forbidden())
		return
	}

	logrus.Println(fmt.Sprintf("%x", (hash.Sum([]byte(signBody)))))
	logrus.Println(ctx.GetApiKey(r))
	logrus.Println(ctx.GetApiSecret(r))

	req.Header.Add("FTX-KEY", ctx.GetApiKey(r))
	req.Header.Add("FTX-TS", strconv.Itoa(curr_time))
	req.Header.Add("FTX-SIGN", hex.EncodeToString(hash.Sum(nil)))
	response, err := http.DefaultClient.Do(req)

	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	w.WriteHeader(200)
	w.Write(body)
}
