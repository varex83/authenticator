package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/varex83/authenticator/internal/service/ctx"
	"gitlab.com/varex83/authenticator/internal/service/tokens"
)

func Refresh(w http.ResponseWriter, r *http.Request) {
	rtokky := ctx.GetRToken(r)
	rtoken, err := ctx.RTokensDB(r).GetByRToken(rtokky)
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}

	token, err := ctx.TokensDB(r).GetByToken(rtoken.Token)
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}

	rtoken.Expiration = tokens.GenExpire(int64(ctx.GetRefreshTokenExp(r)))
	token.Expiration = tokens.GenExpire(int64(ctx.GetTokenExp(r)))

	err = ctx.TokensDB(r).Update(token)
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}
	err = ctx.RTokensDB(r).Update(rtoken)
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.InternalError())
		return
	}

	w.Write([]byte("Success!"))
}
