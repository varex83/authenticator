package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/varex83/authenticator/internal/service/ctx"
	"gitlab.com/varex83/authenticator/internal/service/requests"
	"golang.org/x/crypto/bcrypt"
)

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	account := requests.GetAccount(r)
	bpass, err := bcrypt.GenerateFromPassword([]byte(account.Password), bcrypt.DefaultCost)
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	account.Password = string(bpass)
	res, err := ctx.AccountsDB(r).Insert(account)
	if err != nil {
		ctx.Log(r).Error(err)
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "Error while loggining:"))...)
		return
	}
	ape.Render(w, res)
}
