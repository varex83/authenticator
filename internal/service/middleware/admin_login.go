package middleware

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/varex83/authenticator/internal/data"
	"gitlab.com/varex83/authenticator/internal/service/ctx"
)

func AdminLogin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := ctx.GetToken(r)

		tokky, err := ctx.TokensDB(r).GetByToken(token)
		if err != nil {
			logrus.Error(err)
			ape.RenderErr(w, problems.Unauthorized())
			return
		}

		account, err := ctx.AccountsDB(r).GetByID(tokky.ID)
		if err != nil {
			logrus.Error(err)
			ape.RenderErr(w, problems.InternalError())
			return
		}

		if account.Role == data.Admin {
			next.ServeHTTP(w, r)
			return
		} else {
			logrus.Error(err)
			ape.RenderErr(w, problems.NotAllowed())
			return
		}
	})
}
