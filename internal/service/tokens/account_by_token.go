package tokens

import (
	"net/http"

	"gitlab.com/varex83/authenticator/internal/data"
	"gitlab.com/varex83/authenticator/internal/service/ctx"
)

func GetAccountByToken(r *http.Request, token string) (data.Account, error) {
	tokky, err := ctx.TokensDB(r).GetByToken(token)
	if err != nil {
		return data.Account{}, err
	}
	account, err := ctx.AccountsDB(r).GetByID(tokky.ID)
	if err != nil {
		return data.Account{}, err
	}
	return account, nil
}
