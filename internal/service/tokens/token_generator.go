package tokens

import (
	"crypto/sha256"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/varex83/authenticator/internal/data"
	"gitlab.com/varex83/authenticator/internal/service/ctx"
)

// generates token string
func genTokky(acc data.Account, ttype data.TokenType) string {
	seed := time.Now().Unix() + rand.Int63()
	temp := []byte(strconv.FormatInt(seed, 10) + acc.Email + acc.Password)
	hasher := sha256.New()
	hasher.Write(temp)
	res := hasher.Sum(nil)
	tokky := fmt.Sprintf(string(ttype), res)
	return tokky
}

// generates expiration time
func GenExpire(lifetime int64) int64 {
	return time.Now().Unix() + lifetime
}

// generates token pair
func GenTokenPair(r *http.Request, acc data.Account) data.TokenPair {
	tokenToken := genTokky(acc, data.TokenKey)
	tokenRefreshToken := genTokky(acc, data.RefreshTokenKey)
	return data.TokenPair{
		Token: data.Token{
			ID:         acc.ID,
			Token:      tokenToken,
			Expiration: GenExpire(int64(ctx.GetTokenExp(r))),
		},
		RefreshToken: data.RefreshToken{
			Token:        tokenToken,
			Expiration:   GenExpire(int64(ctx.GetRefreshTokenExp(r))),
			RefreshToken: tokenRefreshToken,
		},
	}
}
