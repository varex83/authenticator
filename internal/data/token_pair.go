package data

type TokenType string

const (
	TokenKey        TokenType = "tt%x"
	RefreshTokenKey TokenType = "rt%x"
)

type TokenPair struct {
	Token        Token
	RefreshToken RefreshToken
}
