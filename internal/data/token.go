package data

import (
	"math/rand"
	"time"
)

type Token struct {
	ID         int64  `db:"id"`
	Token      string `db:"token"`
	Expiration int64  `db:"expiration"`
}

type RefreshToken struct {
	Token        string `db:"token"`
	RefreshToken string `db:"refresh_token"`
	Expiration   int64  `db:"expiration"`
}

type TokenQ interface {
	Insert(token Token) error
	Delete() error
	Update(token Token) error

	Get(id uint64) ([]Token, error)
	GetByToken(token string) (Token, error)

	Reset()

	//Filter all blobs that has expiration pertiod that less than timestamp
	FilterByTimeL(timestamp uint64)
	FilterById(id int64)
}

type RTokenQ interface {
	Insert(token RefreshToken) error
	Delete() error
	Update(rtoken RefreshToken) error

	Get(id uint64) (RefreshToken, error)
	GetByToken(token string) (RefreshToken, error)
	GetByRToken(token string) (RefreshToken, error)

	Reset()

	//Filter all blobs that has expiration pertiod that less than timestamp
	FilterByTimeL(timestamp uint64)
	FilterByToken(token string)
	FilterByRToken(token string)
}

func init() {
	rand.Seed(time.Now().Unix())
}
