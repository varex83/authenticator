package pg

import (
	"context"
	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/varex83/authenticator/internal/data"
)

const (
	accountsTableName = "accounts"
)

type accountQ struct {
	db *pgdb.DB
}

func NewAccountQ(db *pgdb.DB) data.AccountQ {
	return &accountQ{
		db: db,
	}
}

func (q *accountQ) Insert(account data.Account) (data.Account, error) {
	var res data.Account

	clauses := make(map[string]interface{})
	clauses["email"] = account.Email
	clauses["pass"] = account.Password
	clauses["role"] = account.Role

	stmt := sq.Insert(accountsTableName).SetMap(clauses).Suffix("returning *")
	err := q.db.Get(&res, stmt)

	return res, err
}

func (q *accountQ) Update(account data.Account) error {
	clauses := make(map[string]interface{})
	clauses["email"] = account.Email
	clauses["pass"] = account.Password
	clauses["role"] = account.Role

	idstmt := sq.Eq{"id": account.ID}
	stmt := sq.Update(accountsTableName).Where(idstmt).SetMap(clauses)
	err := q.db.ExecContext(context.Background(), stmt)
	if err == sql.ErrNoRows {
		return nil
	}
	return err
}

func (q *accountQ) Get(email string) (data.Account, error) {
	var res data.Account

	stmt := sq.Eq{"email": email}
	err := q.db.Get(&res, sq.Select("*").From(accountsTableName).Where(stmt))

	return res, err
}

func (q *accountQ) GetByID(id int64) (data.Account, error) {
	var res data.Account

	stmt := sq.Eq{"id": id}
	err := q.db.Get(&res, sq.Select("*").From(accountsTableName).Where(stmt))

	return res, err
}
