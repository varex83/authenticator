package pg

import (
	"context"
	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/varex83/authenticator/internal/data"
)

const (
	rTokensTableName = "refresh_tokens"
)

type rTokenQ struct {
	db            *pgdb.DB
	deleteBuilder sq.DeleteBuilder
}

func NewRTokenQ(db *pgdb.DB) data.RTokenQ {
	return &rTokenQ{
		db:            db,
		deleteBuilder: sq.Delete(rTokensTableName),
	}
}

func (q *rTokenQ) Insert(token data.RefreshToken) error {
	clauses := make(map[string]interface{})
	clauses["token"] = token.Token
	clauses["refresh_token"] = token.RefreshToken
	clauses["expiration"] = token.Expiration

	err := q.db.ExecContext(context.Background(), sq.Insert(rTokensTableName).SetMap(clauses))

	return err
}

func (q *rTokenQ) Delete() error {
	err := q.db.Exec(q.deleteBuilder)
	return err
}

func (q *rTokenQ) Update(token data.RefreshToken) error {
	clauses := make(map[string]interface{})
	clauses["expiration"] = token.Expiration

	idstmt := sq.Eq{"refresh_token": token.RefreshToken}
	stmt := sq.Update(rTokensTableName).Where(idstmt).SetMap(clauses)
	err := q.db.ExecContext(context.Background(), stmt)
	if err == sql.ErrNoRows {
		return nil
	}
	return err
}

func (q *rTokenQ) Get(id uint64) (data.RefreshToken, error) {
	var result data.RefreshToken

	stmt := sq.Eq{"id": id}
	err := q.db.Get(&result, sq.Select("*").From(rTokensTableName).Where(stmt))
	return result, err
}

func (q *rTokenQ) GetByToken(token string) (data.RefreshToken, error) {
	var result data.RefreshToken

	stmt := sq.Eq{"token": token}
	err := q.db.Get(&result, sq.Select("*").From(rTokensTableName).Where(stmt))
	return result, err
}
func (q *rTokenQ) GetByRToken(token string) (data.RefreshToken, error) {
	var result data.RefreshToken

	stmt := sq.Eq{"refresh_token": token}
	err := q.db.Get(&result, sq.Select("*").From(rTokensTableName).Where(stmt))
	return result, err
}

func (q *rTokenQ) FilterByTimeL(timestamp uint64) {
	stmt := sq.LtOrEq{"expiration": timestamp}
	q.deleteBuilder = q.deleteBuilder.Where(stmt)
}
func (q *rTokenQ) FilterByToken(token string) {
	stmt := sq.Eq{"token": token}
	q.deleteBuilder = q.deleteBuilder.Where(stmt)
}
func (q *rTokenQ) FilterByRToken(token string) {
	stmt := sq.Eq{"refresh_token": token}
	q.deleteBuilder = q.deleteBuilder.Where(stmt)
}

func (q *rTokenQ) Reset() {
	q.deleteBuilder = sq.Delete(rTokensTableName)
}
