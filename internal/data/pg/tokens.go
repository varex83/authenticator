package pg

import (
	"context"
	"database/sql"

	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/varex83/authenticator/internal/data"
)

const (
	tokensTableName = "tokens"
)

type tokenQ struct {
	db            *pgdb.DB
	deleteBuilder sq.DeleteBuilder
}

func NewTokenQ(db *pgdb.DB) data.TokenQ {
	return &tokenQ{
		db:            db,
		deleteBuilder: sq.Delete(tokensTableName),
	}
}

func (q *tokenQ) Insert(token data.Token) error {
	clauses := structs.Map(token)

	err := q.db.ExecContext(context.Background(), sq.Insert(tokensTableName).SetMap(clauses))

	return err
}

func (q *tokenQ) Update(token data.Token) error {
	clauses := make(map[string]interface{})
	clauses["expiration"] = token.Expiration

	idstmt := sq.Eq{"id": token.ID}
	stmt := sq.Update(tokensTableName).Where(idstmt).SetMap(clauses)
	err := q.db.ExecContext(context.Background(), stmt)
	if err == sql.ErrNoRows {
		return nil
	}
	return err
}

func (q *tokenQ) Delete() error {
	err := q.db.Exec(q.deleteBuilder)
	return err
}

func (q *tokenQ) Get(id uint64) ([]data.Token, error) {
	var result []data.Token

	stmt := sq.Eq{"id": id}
	err := q.db.Select(&result, sq.Select("*").From(tokensTableName).Where(stmt))
	return result, err
}

func (q *tokenQ) GetByToken(token string) (data.Token, error) {
	var result data.Token

	stmt := sq.Eq{"token": token}
	err := q.db.Get(&result, sq.Select("*").From(tokensTableName).Where(stmt))
	return result, err
}

func (q *tokenQ) FilterByTimeL(timestamp uint64) {
	stmt := sq.LtOrEq{"expiration": timestamp}
	q.deleteBuilder = q.deleteBuilder.Where(stmt)
}
func (q *tokenQ) FilterById(id int64) {
	stmt := sq.Eq{"id": id}
	q.deleteBuilder = q.deleteBuilder.Where(stmt)
}

func (q *tokenQ) Reset() {
	q.deleteBuilder = sq.Delete(tokensTableName)
}
