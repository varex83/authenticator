package data

type AccountQ interface {
	Insert(account Account) (Account, error)
	Update(account Account) error
	Get(email string) (Account, error)
	GetByID(id int64) (Account, error)
}

type Role int

const (
	General Role = iota
	Admin
)

type Account struct {
	ID       int64  `db:"id"`
	Email    string `db:"email"`
	Password string `db:"pass"`
	Role     Role   `db:"role"`
}
